package main

import (
	"fmt"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

var (
	app    = kingpin.New("chameleoncolors", "Printcolors for the chameleon lib.")
	fcolor = app.Command("fcolor", "Print foregroundcolors")
	bcolor = app.Command("bcolor", "Print backgroundcolors")
	colors = make(map[int]string)
)

func main() {
	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case fcolor.FullCommand():
		foreground()
	case bcolor.FullCommand():
		background()
	}
}

func foreground() {
	gencolor("38;5;")
}

func background() {
	gencolor("48;5;")
}

func gencolor(prefix string) {
	for i := 0; i < 257; i++ {
		colors[i] = fmt.Sprintf("\033[%s%dm", prefix, i)
	}
	output(prefix)
}

func output(prefix string) {
	y := 0
	out := ""

	for i := 0; i < 257; i++ {
		out = fmt.Sprintf("%s%s%s%dm\x1b[0m \t", out, colors[i], prefix, i)
		y++
		if y == 10 {
			fmt.Println(out)
			out = ""
			y = 0
		}
	}
	fmt.Println(out)
}
